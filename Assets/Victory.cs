﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Victory : MonoBehaviour {

    public Sprite[] winner;
    public Image winSpriteLocation;
    public Text winnerText;

	// Use this for initialization
	void Start () {

        

        if(Player2Respawn.P2numLives < 0) //If player 1 WON
        {
            winnerText.text = "Player 1 wins!";

            switch (CharacterSelector.P1)
            {
                case 0:
                    winSpriteLocation.sprite = winner[0];
                    break;
                case 1:
                    winSpriteLocation.sprite = winner[1];
                    break;
                case 2:
                    winSpriteLocation.sprite = winner[2];
                    break;
                case 3:
                    winSpriteLocation.sprite = winner[3];
                    break;
                default:
                    Debug.Log("P1 winner sprite default bug");
                    break;
            }
        }


        if(Player1Respawn.P1numLives < 0) //If player 2 WON
        {
            winnerText.text = "Player 2 wins!";

            switch (CharacterSelector.P2)
            {
                case 0:
                    winSpriteLocation.sprite = winner[0];
                    break;
                case 1:
                    winSpriteLocation.sprite = winner[1];
                    break;
                case 2:
                    winSpriteLocation.sprite = winner[2];
                    break;
                case 3:
                    winSpriteLocation.sprite = winner[3];
                    break;
                default:
                    Debug.Log("P2 winner sprite default bug");
                    break;
            }
        }

        winSpriteLocation.GetComponent<Image>().SetNativeSize();

	}

    public void Rematch()
    {
        Player1Respawn.P1numLives = 4;
        Player2Respawn.P2numLives = 4;
        SceneManager.LoadScene("Intro");
    }

}
