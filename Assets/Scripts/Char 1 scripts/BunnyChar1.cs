﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class BunnyChar1 : MonoBehaviour {

    Rigidbody2D rb;
    public int jumpPower = 500;
    float moveX;
    public float playerSpeed = 12f;
    public static bool grounded = true;
    bool doublejump = true;
    float djumpdelay = .5f; //short delay before the character can jump twice
    public GameObject player;
    public GameObject attackTrigger;
    public static bool P1rightFace;
    public static bool P2rightFace;
    public Animator BunnyMan;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
        
	}
	
	// Update is called once per frame
	void Update () {
        PMove();
        djumpdelay -= Time.deltaTime; //doublejump delay constantly lowering

        //Checks if the player has gone too far down/left/up/right
        if (gameObject.transform.position.y < -10 || gameObject.transform.position.y > 15 ||  gameObject.transform.position.x < -20 || gameObject.transform.position.x > 20)
        {
            playerDeath();
        }
        
	}

    public void PMove()
    {
        if (CharacterSelector.P1 == 0)
        {
            moveX = Input.GetAxis("Horizontal"); // Need to look into 2 player control schemes
            if (moveX < 0.0f) //moveX is -1 for left, +1 for right (some decimal in between for half-analogue push on controller)
            {
                P1rightFace = false;
                GetComponent<SpriteRenderer>().flipX = true; //Flips sprite if the player is moving left
                                                             //Keep the attack trigger in front of the player (right or left depending on direction faced)
                attackTrigger.transform.position = new Vector2((transform.position.x + -.5f), transform.position.y);
            }
            else if (moveX > 0.0f)
            {
                P1rightFace = true;
                GetComponent<SpriteRenderer>().flipX = false;
                attackTrigger.transform.position = new Vector2((transform.position.x - -.5f), transform.position.y);
            }
        }
        else if (CharacterSelector.P2 == 0)
        {
            moveX = Input.GetAxis("P2Horizontal"); // Need to look into 2 player control schemes
            if (moveX < 0.0f) //moveX is -1 for left, +1 for right (some decimal in between for half-analogue push on controller)
            {
                P2rightFace = false;
                GetComponent<SpriteRenderer>().flipX = true; //Flips sprite if the player is moving left
                                                             //Keep the attack trigger in front of the player (right or left depending on direction faced)
                attackTrigger.transform.position = new Vector2((transform.position.x + /* lazy */ -.5f), transform.position.y);
            }
            else if (moveX > 0.0f)
            {
                P2rightFace = true;
                GetComponent<SpriteRenderer>().flipX = false;
                attackTrigger.transform.position = new Vector2((transform.position.x - -.5f), transform.position.y);
            }
        }

        if ((CharacterSelector.P1 == 0 || CharacterSelector.P2 == 0) && moveX != 0)
        {
            BunnyMan.SetBool("IsMoving", true);
            
        } else if ((CharacterSelector.P1 == 0 || CharacterSelector.P2 == 0) && moveX == 0)
        {
            BunnyMan.SetBool("IsMoving", false);
        }


        //Adds X velocity, multiplying X direction (1 when button is pressed) by playerSpeed. Y velocity remains the same
        //Jump height isn't affected by movespeed
        rb.velocity = new Vector2(moveX * playerSpeed, rb.velocity.y);

        jump(); //run jump method


    }



    void jump()
    {
        if (CharacterSelector.P1 == 0)
        {


            //Jump script:
            if (Input.GetButtonDown("Jump") && grounded == true)
            {
                rb.AddForce(Vector2.up * jumpPower);
                grounded = false;
                djumpdelay = 0.2f; //jump delay prevents both jumps happening simultaneously 
            }

            else if (Input.GetButtonDown("Jump") && doublejump == true && djumpdelay <= 0)
            {
                //Reset vertical velocity for a frame so the double jump has full effect
                rb.velocity = new Vector2(rb.velocity.x, 0);
                //Make the doublejump slightly higher than a regular jump
                rb.AddForce(Vector2.up * (jumpPower * 1.3f));
                doublejump = false;
            }
            BunnyMan.SetBool("IsJumping", !grounded);
        }

        else if (CharacterSelector.P2 == 0)
        {


            //Jump script:
            if (Input.GetButtonDown("P2Jump") && grounded == true)
            {
                rb.AddForce(Vector2.up * jumpPower);
                grounded = false;
                djumpdelay = 0.2f; //jump delay prevents both jumps happening simultaneously 
            }

            else if (Input.GetButtonDown("P2Jump") && doublejump == true && djumpdelay <= 0)
            {
                //Reset vertical velocity for a frame so the double jump has full effect
                rb.velocity = new Vector2(rb.velocity.x, 0);
                //Make the doublejump slightly higher than a regular jump
                rb.AddForce(Vector2.up * (jumpPower * 1.3f));
                doublejump = false;
            }
            BunnyMan.SetBool("IsJumping", !grounded);
        }
      

    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        grounded = true; //Sets grounded when player collides with anything
        //This means players can jump if they clash in the air, or jump off each others' heads

        if (col.gameObject.tag == "Ground")
        { //Resets jump/doublejump capabilities when the character lands
            doublejump = true;
        }
    }

    void playerDeath()
    {
        CameraShaker.Instance.ShakeOnce(10f, 30f, 0.5f, 0.5f);
        Destroy(gameObject);
        SoundManagerScript.PlaySound("Death");
        rb.velocity = new Vector2(0, 0);
        //runs a camera shake script on death (import from asset store)
    }


}


