﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AirDash : MonoBehaviour {

    /*Overcomplicated stolen code from a tutorial
    Really quite an excessive solution, but my own velocity/vector methods weren't working*/

    [SerializeField] float speed, delay;
    [HideInInspector] public bool startDelay;
    int rightPress, leftPress;
    float timePassed, timePassedPress;
    bool startTimer;
    public ConstantForce2D cf;
    float cooldown = 7f;

    private void Start()
    {
        rightPress = 0;
        leftPress = 0;
    }

    private void Update()
    {
        if (CharacterSelector.P1 == 2)
        {
            if (Input.GetButtonDown("Horizontal") && char3script.P1rightFace == false)
            {
                leftPress++;
                startTimer = true;
            }
            else if (Input.GetButtonDown("Horizontal") && char3script.P1rightFace == true)
            {
                rightPress++;
                startTimer = true;
            }
        }
        else if (CharacterSelector.P2 == 2)
        {
            if (Input.GetButtonDown("P2Horizontal") && char3script.P2rightFace == false)
            {
                leftPress++;
                startTimer = true;
            }
            else if (Input.GetButtonDown("P2Horizontal") && char3script.P2rightFace == true)
            {
                rightPress++;
                startTimer = true;
            }
        }

        if (startTimer)
        {
            timePassedPress += Time.deltaTime;
            if(timePassedPress >= 0.3) //Delay press
            {
                startTimer = false;
                leftPress = 0;
                rightPress = 0;
                timePassedPress = 0;
            }
        }

        if (leftPress >= 2 || rightPress >= 2)
        {
            startDelay = true;  
        }
        cooldown += Time.deltaTime;

    }

    public void FixedUpdate()

    {


        if (startDelay)
        {

            timePassed += Time.deltaTime;
            
            if (timePassed > delay)
            {
                if (rightPress >= 2 && cooldown >= 4)
                {
                    SoundManagerScript.PlaySound("dash");
                    timePassed = 0;
                    cf.force = new Vector2(speed, 0);
                    rightPress = 0;
                    cooldown = 0;


                }
                else if (leftPress >= 2 && cooldown >= 4)
                {
                    SoundManagerScript.PlaySound("dash");
                    timePassed = 0;
                    cf.force = new Vector2(-speed, 0);
                    leftPress = 0;
                    cooldown = 0;

                }
            }
            else
            { 
                timePassed = 0;
                startDelay = false;
                rightPress = 0;
                leftPress = 0;
                delay = 0;

            }

            if (timePassed >= 0.15f)
            {
                cf.force = new Vector2(0, 0);

            }

        }
    }

}
