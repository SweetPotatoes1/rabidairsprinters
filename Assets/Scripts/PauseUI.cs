﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseUI : MonoBehaviour {

    public Canvas PauseOverlay;
    bool paused = false;

    private void Start()
    {
        PauseOverlay.enabled = false;
        paused = false;
        Time.timeScale = 1;
    }

    public void Resume()
    {
        PauseOverlay.enabled = false;
        Time.timeScale = 1;
        paused = false;
    }

    public void Restart()
    {
        
        SceneManager.LoadScene("Intro");
        Time.timeScale = 1;

    }

    public void Buttonquit()
    {
        Debug.Log("Quit");
        Application.Quit();
    }

    private void Update()
    {
        if(Input.GetButtonDown("Cancel") && paused == false)
        {
            paused = true;
            PauseOverlay.enabled = true;
            Time.timeScale = 0;
        }
        else if (Input.GetButtonDown("Cancel") && paused == true)
        {
            paused = false;
            PauseOverlay.enabled = false;
            Time.timeScale = 1;
        }
    }

}
