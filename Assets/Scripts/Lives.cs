﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Lives : MonoBehaviour {

    public Sprite[] Char1Sprites;
    public Sprite[] Char2Sprites;
    public Sprite[] Char3Sprites;
    public Sprite[] Char4Sprites;
    public Image player1Lives;
    public Image player2Lives;
    public Text strengthDisplay;


    private void Update()
    {


        switch (CharacterSelector.P1)
        {
            
            case 0:
                player1Lives.sprite = Char1Sprites[Player1Respawn.P1numLives];
                break;
            case 1:
                player1Lives.sprite = Char2Sprites[Player1Respawn.P1numLives];
                break;
            case 2:
                player1Lives.sprite = Char3Sprites[Player1Respawn.P1numLives];
                break;
            case 3:
                player1Lives.sprite = Char4Sprites[Player1Respawn.P1numLives];
                break;
            default:
                Debug.Log("P1 lives default case error");
                break;
        }

        switch (CharacterSelector.P2)
        {
            case 0:
                player2Lives.sprite = Char1Sprites[Player2Respawn.P2numLives];
                break;
            case 1:
                player2Lives.sprite = Char2Sprites[Player2Respawn.P2numLives];
                break;
            case 2:
                player2Lives.sprite = Char3Sprites[Player2Respawn.P2numLives];
                break;
            case 3:
                player2Lives.sprite = Char4Sprites[Player2Respawn.P2numLives];
                break;
            default:
                Debug.Log("P2 lives default case error");
                break;
        }

        strengthDisplay.GetComponent<Text>().text = attackTrigg.XstrengthString;

        strengthDisplay.GetComponent<Text>().fontSize = (int)(attackTrigg.fakeStrength * 3) + 30;

    }

}
