﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour {

    static AudioSource audioSrc;
    public static AudioClip Normalhit,Jump, Tail, Thud, dash, Mdeath, NinjaDeath, CactusSpecial, fade, Papa, reverse_oof, KickHit, Death, onepunch, onekarate;
	void Start () {

        Normalhit = Resources.Load<AudioClip>("NormalHit");
        reverse_oof = Resources.Load<AudioClip>("reverse_oof");
        KickHit = Resources.Load<AudioClip>("KickHit");
        Death = Resources.Load<AudioClip>("Death");
        onepunch = Resources.Load<AudioClip>("onepunch");
        onekarate = Resources.Load<AudioClip>("onekarate");
        Jump = Resources.Load<AudioClip>("Jump");
        Tail = Resources.Load<AudioClip>("Tail");
        Thud = Resources.Load<AudioClip>("Thud");
        dash = Resources.Load<AudioClip>("dash");
        Mdeath = Resources.Load<AudioClip>("Mdeath");
        fade = Resources.Load<AudioClip>("fade");
        CactusSpecial = Resources.Load<AudioClip>("CactusSpecial");
        Papa = Resources.Load<AudioClip>("Papa");
        NinjaDeath = Resources.Load<AudioClip>("NinjaDeath");

        audioSrc = GetComponent<AudioSource>();

    }
	
	// Update is called once per frame
	void Update () {
		
}
    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "NormalHit":
                audioSrc.PlayOneShot(Normalhit);
                break;
            case "KickHit":
                audioSrc.PlayOneShot(KickHit);
                break;
            case "reverse_oof":
                audioSrc.PlayOneShot(reverse_oof);
                break;
            case "Death":
                audioSrc.PlayOneShot(Death);
                break;
            case "onepuch":
                audioSrc.PlayOneShot(onepunch);
                break;
            case "onekarate":
                audioSrc.PlayOneShot(onekarate);
                break;
            case "CactusSpecial":
                audioSrc.PlayOneShot(CactusSpecial);
                break;
            case "Papa":
                audioSrc.PlayOneShot(Papa);
                break;
            case "NinjaDeath":
                audioSrc.PlayOneShot(NinjaDeath);
                break;
            case "Jump":
                audioSrc.PlayOneShot(Jump);
                break;
            case "Tail":
                audioSrc.PlayOneShot(Tail);
                break;
            case "dash":
                audioSrc.PlayOneShot(dash);
                break;
            case "Thud":
                audioSrc.PlayOneShot(Thud);
                break;
            case "fade":
                audioSrc.PlayOneShot(fade);
                break;
            case "Mdeath":
                audioSrc.PlayOneShot(Mdeath);
                break;
            
        } 
        
           
    }
}
