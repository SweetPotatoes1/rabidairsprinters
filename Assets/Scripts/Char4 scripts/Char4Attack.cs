﻿using UnityEngine;

public class Char4Attack : MonoBehaviour {

    private bool attacking = false;
    private float attackTimer = 0f;
    private float attackCD = .4f;
    public Animator animator;
    public float cooldown = 10f;
    private bool attacking2 = false;

    public Collider2D attackTrigger;
    public Collider2D circleTrigger;

    private void Awake()
    {
        attackTrigger.enabled = false;
        circleTrigger.enabled = false;
        cooldown = 10f;
    }

    private void Update()
    {
        cooldown += Time.deltaTime;
        if (CharacterSelector.P1 == 3)
        {
            if(Input.GetButtonDown("Fire") && !attacking)
            {
                if (!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("onekarate");
                }
                else
                {
                    SoundManagerScript.PlaySound("onekarate");
                }
                attacking = true;
                AttackAnim();
                attackTimer = attackCD;
                attackTrigger.enabled = true;
            }

            if (attacking)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking = false;
                    attackTrigger.enabled = false;
                }
            }
        }
            
        else if (CharacterSelector.P2 == 3)
        {
            if (Input.GetButtonDown("P2Fire") && !attacking)
            {
                if (!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("onekarate");
                }
                else
                {
                    SoundManagerScript.PlaySound("onekarate");

                }
                attacking = true;
                attackTimer = attackCD;
                AttackAnim();
                attackTrigger.enabled = true;
            }
            if (attacking)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking = false;
                    attackTrigger.enabled = false;
                }
            }
        }

        

        if (CharacterSelector.P1 == 3)
        {
            if (Input.GetButtonDown("Fire2") && cooldown >= 10){
                animator.SetBool("IsSpecial", true);
                attacking2 = true;

                attackTimer = attackCD;
                circleTrigger.enabled = true;
                cooldown = 0;
            }
            if (attacking2)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking2 = false;
                    circleTrigger.enabled = false;
                    cooldown = 0;
                 
                }
            }
            
        }

        else if (CharacterSelector.P2 == 3)
        {
            if (Input.GetButtonDown("P2Fire2") && cooldown >= 10)
            {
                attacking2 = true;
                animator.SetBool("IsSpecial", true);
                attackTimer = attackCD;
                circleTrigger.enabled = true;
                cooldown = 0;
            }
            if (attacking2)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking2 = false;
                    circleTrigger.enabled = false;
                    cooldown = 0;
                   
                }
            }

        }
        Debug.Log(cooldown);
        animator.SetFloat("CD", cooldown);  

        if(attackTimer <= 0)
        {
            AttackAnim();
            animator.SetBool("IsSpecial", false);
            attacking2 = false;
            attacking = false;
            attackTrigger.enabled = false;
            circleTrigger.enabled = false;
        }
    }

    void AttackAnim()
    {
        animator.SetBool("IsAttacking", attacking);
    }
 
         

}
