﻿using UnityEngine;
using EZCameraShake;

public class CactusTrigAtt : MonoBehaviour {

    private GameObject opponent;
    public GameObject cactus;

    [HideInInspector]
    public float resetTimer = .2f;



    private void Start()    {
        resetTimer = .2f;


    }

    private void Update()
    {
        resetTimer -= Time.deltaTime;

        if (resetTimer <= 0 && opponent != null)
        {
            EnableScripts();
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (CharacterSelector.P1 == 3 && col.gameObject.tag != "Ground" && col.gameObject.tag != "Cactus")
        {
            opponent = col.gameObject;
            DisableScripts();
            Special();
            resetTimer = .5f;
            SoundManagerScript.PlaySound("NormalHit");
        }

        if (CharacterSelector.P2 == 3 && col.gameObject.tag != "Ground" && col.gameObject.tag != "Cactus")
        {
            opponent = col.gameObject;
            DisableScripts();
            Special();
            resetTimer = .5f;
            SoundManagerScript.PlaySound("NormalHit");
        }
    }

    void DisableScripts()
    {
        if (opponent.tag == "Bunny") { opponent.GetComponent<BunnyChar1>().enabled = false; }
        else if (opponent.tag == "Inv" && (char2ability.invuln == false)) { opponent.GetComponent<Char2>().enabled = false; }
        else if (opponent.tag == "Dash")
        {
            opponent.GetComponent<char3script>().enabled = false;
            opponent.GetComponent<AirDash>().enabled = false;
        }
        
    }

    void EnableScripts()
    {
        if (opponent.tag == "Bunny")
        {
            opponent.GetComponent<BunnyChar1>().enabled = true;
        }
        else if (opponent.tag == "Inv")
        {
            opponent.GetComponent<Char2>().enabled = true;
        }
        else if (opponent.tag == "Dash")
        {
            opponent.GetComponent<char3script>().enabled = true;
            opponent.GetComponent<AirDash>().enabled = true;
        }
        opponent = null;

    }



    public void Special()
    {
        
        
        if (char2ability.invuln == false)
        {

            
            if (cactus.transform.position.x < opponent.transform.position.x && cactus.transform.position.y < (opponent.transform.position.y - .6f))
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(attackTrigg.Xstrength / 2, attackTrigg.Ystrength * 1.7f);
                if(!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("Thud");

                }
            


            }
            else if (cactus.transform.position.x < opponent.transform.position.x)
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(attackTrigg.Xstrength, attackTrigg.Ystrength);
                if (!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("Thud");

                }


            }

            if (cactus.transform.position.x > opponent.transform.position.x && cactus.transform.position.y < (opponent.transform.position.y - .6f))
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-attackTrigg.Xstrength / 2, attackTrigg.Ystrength * 1.7f);
                if (!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("Thud");

                }


            }
            else if (cactus.transform.position.x > opponent.transform.position.x)
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-attackTrigg.Xstrength, attackTrigg.Ystrength);
                if (!Char4script.grounded)
                {
                    SoundManagerScript.PlaySound("Thud");

                }


            }

            CameraShaker.Instance.ShakeOnce(5f, 10f, 0.3f, 0.5f);
            
            strengthUpdate();



        }

    }

    public void strengthUpdate()
    {
        if (attackTrigg.Xstrength / attackTrigg.Ystrength == 2)
        {
            if (attackTrigg.fakeStrength <= 10) //strength increases faster at the start
            {
                attackTrigg.Xstrength += 2;
            }
            else
            {
                attackTrigg.Xstrength++;
            }

            attackTrigg.XstrengthString = string.Format("{0}", attackTrigg.fakeStrength);

            attackTrigg.Ystrength = attackTrigg.Xstrength / 2;
        }
    }
}
