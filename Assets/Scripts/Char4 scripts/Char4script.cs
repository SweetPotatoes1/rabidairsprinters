﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZCameraShake;

public class Char4script : MonoBehaviour {

    Rigidbody2D rb;
    public int jumpPower = 500;
    float moveX;
    public float playerSpeed = 12f;
    public static bool grounded = true;
    public GameObject player;
    public GameObject attackTrigger;
    public static bool P1rightFace;
    public static bool P2rightFace;
    public Animator animator;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update() {
        PMove();

        //Checks if the player has gone too far down/left/up/right
        if (gameObject.transform.position.y < -10 || gameObject.transform.position.y > 13 || gameObject.transform.position.x < -20 || gameObject.transform.position.x > 20)
        {
            playerDeath();
        }

    }

    void PMove()
    {
        if (CharacterSelector.P1 == 3)
        {
            moveX = Input.GetAxis("Horizontal"); // Need to look into 2 player control schemes
            if (moveX < 0.0f) //moveX is -1 for left, +1 for right (some decimal in between for half-analogue push on controller)
            {
                P1rightFace = false;
                GetComponent<SpriteRenderer>().flipX = true; //Flips sprite if the player is moving left
                attackTrigger.transform.position = new Vector2(transform.position.x + -.5f, transform.position.y);

            }
            else if (moveX > 0.0f)
            {
                P1rightFace = true;
                GetComponent<SpriteRenderer>().flipX = false;
                attackTrigger.transform.position = new Vector2(transform.position.x - -.5f, transform.position.y);
            }
        }
        else if (CharacterSelector.P2 == 3)
        {
            moveX = Input.GetAxis("P2Horizontal"); //Badly duplicated code for P2 controls. Method should be used.

            if (moveX < 0.0f)
            {
                P2rightFace = false;
                GetComponent<SpriteRenderer>().flipX = true;
                attackTrigger.transform.position = new Vector2(transform.position.x + -.5f, transform.position.y);

            }
            else if (moveX > 0.0f)
            {
                P2rightFace = true;
                GetComponent<SpriteRenderer>().flipX = false;
                attackTrigger.transform.position = new Vector2(transform.position.x - -.5f, transform.position.y);
            }
        }

        if ((CharacterSelector.P1 == 3 || CharacterSelector.P2 == 3) && moveX != 0)
        {
            animator.SetBool("IsWalking", true);
        }
        else if (moveX == 0)
        {
            animator.SetBool("IsWalking", false);
        }

       

        //Adds X velocity, multiplying X direction (1 when button is pressed) by playerSpeed. Y velocity remains the same
        //Jump height isn't affected by movespeed
        rb.velocity = new Vector2(moveX * playerSpeed, rb.velocity.y);

        jump(); //run jump method


    }



    void jump()
    {
        if (CharacterSelector.P1 == 3)
        {
            //Jump script:
            if (Input.GetButtonDown("Jump") && grounded == true)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * jumpPower);
                grounded = false;
            }
        }

        else if (CharacterSelector.P2 == 3)
        {
            //Jump script P2 controls:
            if (Input.GetButtonDown("P2Jump") && grounded == true)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0);
                rb.AddForce(Vector2.up * jumpPower);
                grounded = false;
            }
        }

        animator.SetBool("IsJumping", !grounded);
        
    }


    private void OnCollisionEnter2D(Collision2D col)
    {
        grounded = true;
    }

    void playerDeath()
    {
        CameraShaker.Instance.ShakeOnce(10f, 30f, 0.5f, 0.5f);
        Destroy(gameObject);
        SoundManagerScript.PlaySound("Death");
        rb.velocity = new Vector2(0, 0);

        //runs a camera shake script on death (import from asset store)
    }


}


