﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class char2ability : MonoBehaviour {

    [SerializeField]
    float cooldown = 12f;
    float abilityTimer = 0f;
    public static bool invuln;
    Renderer rend;
    Color c;


    private void Start()
    {
        rend = GetComponent<Renderer>();
        c = rend.material.color;
        invuln = false;

    }

    void Update () {
        abilityTimer -= Time.deltaTime;
        cooldown += Time.deltaTime;
        if (Input.GetButtonDown("Fire2") && CharacterSelector.P1 == 1 && cooldown > 3f)
        {
            SoundManagerScript.PlaySound("fade");
            UseAbility();
        }
        else if (Input.GetButtonDown("P2Fire2") && CharacterSelector.P2 == 1 && cooldown > 3f)
        {
            SoundManagerScript.PlaySound("fade");
            UseAbility();
        }

        if (abilityTimer <= 0 && invuln == true)
        {
            c.a = 1f;
            rend.material.color = c;
            invuln = false;
        }

        

	}


    void UseAbility()
    {
        abilityTimer = 2;
        cooldown = 0;
        c.a = .5f;
        rend.material.color = c;
        invuln = true;
        

        
    }

}
