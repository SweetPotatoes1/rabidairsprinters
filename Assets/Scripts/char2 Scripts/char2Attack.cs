﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class char2Attack : MonoBehaviour {

    //I might add more to this to stop attack spamming, but it's not a big deal

    private bool attacking = false;
    private float attackTimer = 0f;
    private float attackCD = .3f; //how long the attack trigger is active for

    public Collider2D attackTrigger;
    public Animator animator;
 //   private Animator anim; //Not needed until later

    private void Awake()
    {
  //      anim = gameObject.GetComponent<Animator>();
        attackTrigger.enabled = false; //Disables the attacking collider on wake
    }

    private void Update()
    {
        if (CharacterSelector.P1 == 1)
        {
            if (Input.GetButtonDown("Fire") && !attacking)
            {
                if (!Char2.grounded)
                {
                    SoundManagerScript.PlaySound("Tail");
                }
                else
                {
                    SoundManagerScript.PlaySound("onekarate");
                }
                attacking = true;
                attackTimer = attackCD;
                attackTrigger.enabled = true; //enable attack trigger on fire (left shift for p1, num0 for p2)
            }

            if (attacking)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking = false;
                    attackTrigger.enabled = false;
                }
            }
            animator.SetBool("IsAttacking", attacking);
        }
        else if (CharacterSelector.P2 == 1)
        {
            if (Input.GetButtonDown("P2Fire") && !attacking)
            {
                if(!Char2.grounded)
                {
                    SoundManagerScript.PlaySound("Tail");
                }
                else
                {
                    SoundManagerScript.PlaySound("onekarate");
                }
                attacking = true;
                attackTimer = attackCD;
                attackTrigger.enabled = true; //enable attack trigger on fire (F for p1, num0 for p2)
            }

            if (attacking)
            {
                if (attackTimer > 0)
                {
                    attackTimer -= Time.deltaTime;
                }
                else
                {
                    attacking = false;
                    attackTrigger.enabled = false;
                }
            }
            animator.SetBool("IsAttacking", attacking);

        }


            //anim.SetBool("Attacking", attacking); Uncomment when anim is added
        }
}
