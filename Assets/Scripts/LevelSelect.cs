﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class LevelSelect : MonoBehaviour {

    public void BeachSelect()
    {
        SceneManager.LoadScene("Beach");
    }

    public void ForestSelect()
    {
        SceneManager.LoadScene("Forest");
    }

    public void IntroStart()
    {
        SceneManager.LoadScene("P1Select");
    }

    public void ButtonQuit()
    {
        Application.Quit();
    }
    

}
