﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZCameraShake;


public class attackTrigg : MonoBehaviour {

    [SerializeField]
    public static float Xstrength = 4f; //adjustable values depending on the character
    public static float fakeStrength;
    [SerializeField]
    public static float Ystrength = 2f; //Floats to check division results on hit
    private GameObject opponent;


    public static string XstrengthString;

    [HideInInspector]
    public float resetTimer = 0.2f;
    public static GameObject player1;
    public static GameObject player2;

    public GameObject strengthDisplay;

    private void Start()
    {
        resetTimer = .2f;
        fakeStrength = 0;
        XstrengthString = string.Format("{0}", fakeStrength);
    }

    private void Update()
    {
        //Timer used for re-enabling scripts after getting hit
        resetTimer -= Time.deltaTime;

        if (resetTimer <= 0 && opponent != null) //!Null prevents errors from trying to constantly enable scripts
        { //Opponent gets reset to null when scripts get re-enabled
            EnableScripts();
        }
      /*  if (timescaletimer <= 0 && timeSlow == true)
        {
            Time.timeScale = 1;
            timeSlow = false;
        }*/

        fakeStrength = Xstrength - 3; //Set the display strength starting value to 0 (starting strength is actually 4), so this makes Tension start at 0 and increment the same way
    }


    private void OnTriggerEnter2D(Collider2D col)
    {

        for(int i = 0; i <=2; i++) {
            if (CharacterSelector.P1 == i && col.gameObject.tag != "Ground" && col.gameObject != GetComponentInParent<BoxCollider2D>() && col.gameObject.name != "AttackPos")
            {
                opponent = col.gameObject;
                DisableScripts();
                Hit();
                SoundManagerScript.PlaySound("NormalHit");
               
            }
        }

        for (int i = 0; i <= 2; i++)
        {
            if (CharacterSelector.P2 == i && col.gameObject.tag != "Ground" && col.gameObject != GetComponentInParent<BoxCollider2D>() && col.gameObject.name != "AttackPos")
            {
                opponent = col.gameObject;
                DisableScripts();
                Hit();
                SoundManagerScript.PlaySound("NormalHit");
            }
        }


    }

    void DisableScripts()
    {
        //Disables movement scripts of the opponent when hit
        if (opponent.tag == "Bunny") { opponent.GetComponent<BunnyChar1>().enabled = false; }
        else if (opponent.tag == "Inv" && (char2ability.invuln == false)) { opponent.GetComponent<Char2>().enabled = false; }
        else if (opponent.tag == "Dash")
        {
            opponent.GetComponent<char3script>().enabled = false;
            opponent.GetComponent<AirDash>().enabled = false;
        }
        else if (opponent.tag == "Cactus") { opponent.GetComponent<Char4script>().enabled = false; }
        resetTimer = .5f;
       /* Time.timeScale = 0.5f;
        timeSlow = true;
        timescaletimer = 0.3f;*/
    }
    
    void EnableScripts()
    {
        if (opponent.tag == "Bunny")
        {    
            opponent.GetComponent<BunnyChar1>().enabled = true;
        } else if (opponent.tag == "Inv")
        {
            opponent.GetComponent<Char2>().enabled = true;
        } else if (opponent.tag == "Dash")
        {
            opponent.GetComponent<char3script>().enabled = true;
            opponent.GetComponent<AirDash>().enabled = true;
        }
        else if (opponent.tag == "Cactus") { opponent.GetComponent<Char4script>().enabled = true; }

        opponent = null;
    }

    void Hit()
    {
        

        //Takes the X position of the triggers parent object (the player), and compares to the position of the opponent
        if (char2ability.invuln == false) //Hits aren't registered if Phase Shift is active
        {
            if (transform.parent.gameObject.transform.position.x < opponent.transform.position.x) //it works!
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Xstrength, Ystrength);

            }
            else if (transform.parent.gameObject.transform.position.x > opponent.transform.position.x)
            {
                opponent.gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(-Xstrength, Ystrength);
            }

            CameraShaker.Instance.ShakeOnce((Xstrength / 4), 10f, 0.3f, 0.5f);

            //Strength increases, X increases twice as much as Y
            if (Xstrength / Ystrength == 2)
            {
                if(fakeStrength <= 10) //strength increases faster at the start
                {
                    Xstrength += 2;
                }
                else
                {
                    Xstrength++;
                }
                
                XstrengthString = string.Format("{0}", fakeStrength);

                Ystrength = Xstrength / 2;
            }

        }




    }
}
    

