﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player2Respawn : MonoBehaviour {

    public GameObject player2Spawn;
    public static int P2numLives = 4;
    public GameObject[] players;

    void Start () {
        P2numLives = 4;
        P2Respawn();
	}

    private void Update()
    {
        //Keeps the spawner 'patrolling' left and right to prevent spawnkilling
        if (gameObject.transform.position.x >= 5)
        {
            gameObject.GetComponent<ConstantForce2D>().force = new Vector2(-15, 0);
        }
        else if (gameObject.transform.position.x <= -6)
        {
            gameObject.GetComponent<ConstantForce2D>().force = new Vector2(15, 0);
        }

        if (player2Spawn == null && P2numLives >= 0) //Runs if there is no active player
        {
            P2Respawn();
            P2numLives--;
            attackTrigg.XstrengthString = string.Format("{0}", attackTrigg.Xstrength);

        }
        else if (P2numLives < 0)
        {
            SceneManager.LoadScene("Victory");
        }

    }

    void P2Respawn()
    { //Create a new player prefab on the playerspawners location
       
        int selected = PlayerPrefs.GetInt("P2SelectedCharacter");
        player2Spawn = (GameObject)Instantiate(players[selected], transform.position, Quaternion.identity);
        attackTrigg.player2 = players[selected];
        attackTrigg.Xstrength = 4;
        attackTrigg.Ystrength = 2;
    }
}
