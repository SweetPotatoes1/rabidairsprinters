﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player1Respawn : MonoBehaviour {

    public GameObject player1Spawn;
    public static int P1numLives = 4;
    public GameObject[] players;

    void Start () {
        P1numLives = 4;
        P1Respawn();
        Destroy(GameObject.FindGameObjectWithTag("Music"));
	}

    private void Update()
    {
        if(gameObject.transform.position.x >= 5)
        {
            gameObject.GetComponent<ConstantForce2D>().force = new Vector2(-15, 0);
        }
        else if (gameObject.transform.position.x <= -6)
        {
            gameObject.GetComponent<ConstantForce2D>().force = new Vector2(15, 0);
        }

        if (player1Spawn == null && P1numLives >= 0) //Runs if there is no active player
        {

            P1Respawn();
            P1numLives--;
            attackTrigg.XstrengthString = string.Format("{0}", attackTrigg.Xstrength);

        }
        else if(P1numLives < 0)
        {
            SceneManager.LoadScene("Victory");
        }

    }

    void P1Respawn()
    { //Create a new player prefab on the playerspawners location
        int selected = PlayerPrefs.GetInt("P1SelectedCharacter");
        player1Spawn = (GameObject)Instantiate(players[selected], transform.position, Quaternion.identity);
        attackTrigg.player1 = players[selected];
        attackTrigg.Xstrength = 4;
        attackTrigg.Ystrength = 2;
    }

}
