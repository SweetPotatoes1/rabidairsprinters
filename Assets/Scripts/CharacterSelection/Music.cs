﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Music : MonoBehaviour {

    public GameObject music;

    private void Awake()
    {
        if(SceneManager.GetActiveScene().name != "Beach" || (SceneManager.GetActiveScene().name != "Forest"))
        {
            DontDestroyOnLoad(music.gameObject);
        }

    }
}
