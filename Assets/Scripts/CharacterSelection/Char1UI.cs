﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Char1UI : MonoBehaviour {

    public Image panelRight;
    public Text bunnyText;

    public void pointEnter()
    {
        panelRight.enabled = true;
        bunnyText.enabled = true;
    }
    public void pointExit()
    {
        panelRight.enabled = false;
        bunnyText.enabled = false;
    }
}
