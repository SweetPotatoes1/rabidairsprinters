﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Char3UI : MonoBehaviour {

    public Image panelLeft;
    public Text roboText;

    public void pointEnter()
    {
        panelLeft.enabled = true;
        roboText.enabled = true;
    }
    public void pointExit()
    {
        panelLeft.enabled = false;
        roboText.enabled = false;
    }
	
}
