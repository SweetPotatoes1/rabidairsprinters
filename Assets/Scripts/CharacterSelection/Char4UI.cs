﻿using UnityEngine;
using UnityEngine.UI;

public class Char4UI : MonoBehaviour {

    public Image panelLeft;
    public Text cactText;

    public void pointEnter()
    {
        panelLeft.enabled = true;
        cactText.enabled = true;

    }

    public void pointExit()
    {
        panelLeft.enabled = false;
        cactText.enabled = false;
    }
}
