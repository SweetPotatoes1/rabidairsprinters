﻿using UnityEngine;
using UnityEngine.UI;

public class Char2UI : MonoBehaviour {

    public Image panelRight;
    public Text mermaidText;

    public void pointEnter()
    {
        panelRight.enabled = true;
        mermaidText.enabled = true;
    }
    public void pointExit()
    {
        panelRight.enabled = false;
        mermaidText.enabled = false;

    }
}
