﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterSelector : MonoBehaviour {

    public static int P1 = -1;
    public static int P2 = -1;
    public Text errorText;


    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "P1Select")
        {
            PlayerPrefs.DeleteAll();
            P1 = -1;
        } else if (SceneManager.GetActiveScene().name == "P2Select")
        {
            P2 = -1;
        }

            
    }

    public void P1ChooseCharacter(int P1characterIndex)
    {
        PlayerPrefs.SetInt("P1SelectedCharacter", P1characterIndex);

        P1 = P1characterIndex;
    }
    public void P2ChooseCharacter(int P2characterIndex)
    {
        PlayerPrefs.SetInt("P2SelectedCharacter", P2characterIndex);
        P2 = P2characterIndex;
    }

    public void LoadP2Select()
    {
        if (P1 != -1)
        {
            SceneManager.LoadScene("P2Select");
        } else if (P1 == -1)
        {
            errorText.GetComponent<Text>().text = ("You need to pick a hero!");
        } 
    }



    public void LevelSelect()
    {
        if (P1 != P2 && P2 != -1)
        {
            SceneManager.LoadScene("LevelSelect");
        } else if (P2 == -1)
        {
            errorText.GetComponent<Text>().text = ("You need to pick a hero!");
        }
        else if (P1 == P2)
        {
            errorText.GetComponent<Text>().text = ("You cannot be the same hero!");
        }
    }



}
